<?php
class Database
{
    public static function Conectar()
    {
    	$pdo = new PDO('pgsql:host=localhost;dbname=factura;','postgres','123456');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
		$pdo->exec("SET NAMES 'UTF8'");
        return $pdo;
    }
}