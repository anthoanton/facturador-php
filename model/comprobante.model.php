<?php

class ComprobanteModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
            $this->pdo = Database::Conectar();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
            /*listamos todo nuestro comprobante*/
            $stm = $this->pdo->prepare("SELECT comprobante.id, ruc, cliente.nombre as nombre, igv, subtotal, comprobante.total FROM cliente 
                join comprobante on comprobante.cliente_id = cliente.id
                join comprobante_detalle on comprobante_detalle.comprobante_id = comprobante.id
                join producto on producto.id = comprobante_detalle.producto_id GROUP BY comprobante.id, ruc, cliente.nombre, igv, subtotal, comprobante.total ORDER BY comprobante.id DESC");
            $stm->execute();

            foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r)
            {
                $result[] = [
                    'Id'=>$r->id, 
                    'Cedula'=>$r->ruc, 
                    'Cliente'=>$r->nombre,
                    'IGV'=>$r->igv,
                    'SubTotal'=>$r->subtotal,
                    'Total'=>$r->total,
                ];
            }

            return $result;

		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id, $formato)
	{
		try
		{
			$stm = $this->pdo->prepare("SELECT * FROM comprobante WHERE id = ?");
			$stm->execute(array($id));
            
            if ($formato == 'excel') {
                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=mi_archivo.xls");
                header("Pragma: no-cache");
                header("Expires: 0");
            }
            
			$c = $stm->fetch(PDO::FETCH_OBJ);
            
            /* El cliente asignado */
            $c->{'Cliente'} = $this->pdo->query("SELECT * FROM cliente c WHERE c.id = " . $c->cliente_id)->fetch(PDO::FETCH_OBJ);

            /* Traemos el detalle */
            $c->{'Detalle'} = $this->pdo->query("SELECT * FROM comprobante_detalle cd WHERE cd.comprobante_id = " . $c->id)->fetchAll(PDO::FETCH_OBJ);

            foreach($c->Detalle as $k => $d)
            {
                $c->Detalle[$k]->{'Producto'} = $this->pdo->query("SELECT * FROM producto p WHERE p.id = " . $d->producto_id)->fetch(PDO::FETCH_OBJ);
            }
            
            return $c;
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$stm = $this->pdo->prepare("DELETE FROM comprobante WHERE id = ?");
			$stm->execute(array($id));
		}
        catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar($comprobante)
	{
		try 
		{
            /* Registramos el comprobante */
            $sql = "INSERT INTO comprobante(Cliente_id, IGV, SubTotal, Total) VALUES (?, ?, ?, ?);";
            $this->pdo->prepare($sql)
                      ->execute(
                        array(
                            $comprobante['cliente_id'],
                            $comprobante['igv'],
                            $comprobante['subtotal'],
                            $comprobante['total']
                        ));

            /* El ultimo ID que se ha generado */
            $comprobante_id = $this->pdo->lastInsertId();
            
            /* Recorremos el detalle para insertar */
            foreach($comprobante['items'] as $d)
            {
                $sql = "INSERT INTO comprobante_detalle (Comprobante_id,Producto_id,Cantidad,PrecioUnitario,Total) 
                        VALUES (?, ?, ?, ?, ?)";
                
                $this->pdo->prepare($sql)
                          ->execute(
                            array(
                                $comprobante_id,
                                $d['producto_id'],
                                $d['cantidad'],
                                $d['precio'],
                                $d['total']
                            ));
            }
            
            return true;
		}
        catch (Exception $e) 
		{
			return false;
		}
	}

}