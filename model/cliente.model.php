<?php
class ClienteModel
{
	private $pdo;

	public function __CONSTRUCT()
	{
		try
		{
            $this->pdo = Database::Conectar();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Buscar($criterio)
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM cliente WHERE Nombre ILIKE '%$criterio%' ORDER BY Nombre LIMIT 8");
			$stm->execute();

			foreach($stm->fetchAll(PDO::FETCH_OBJ) as $r)
			{
				$result[] = ['id'=>$r->id, 'value'=>$r->nombre, 'ruc'=>$r->ruc, 'direccion'=>$r->direccion];
			}

			return $result;
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
}