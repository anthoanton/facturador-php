--postgres sql
CREATE TABLE IF NOT EXISTS cliente (
  id serial,
  Nombre varchar(100),
  RUC varchar(11),
  Direccion varchar(200),
  PRIMARY KEY (id)
);

-- Volcando estructura para tabla factura.producto postgres sql
CREATE TABLE IF NOT EXISTS producto (
  id serial,
  Nombre varchar(100),
  Precio decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (id)
);

-- Volcando estructura para tabla factura.comprobante postgres sql
CREATE TABLE IF NOT EXISTS comprobante (
  id serial,
  Cliente_id integer,
  IGV decimal(10,2) DEFAULT '0.00',
  SubTotal decimal(10,2) DEFAULT '0.00',
  Total decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (id),
  CONSTRAINT FK_comprobante_cliente FOREIGN KEY (Cliente_id) REFERENCES cliente (id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Volcando estructura para tabla factura.comprobante_detalle postgres sql
CREATE TABLE IF NOT EXISTS comprobante_detalle (
  Comprobante_id integer,
  Producto_id integer,
  Cantidad decimal(10,2) DEFAULT '0.00',
  PrecioUnitario decimal(10,2) DEFAULT '0.00',
  Total decimal(10,2) DEFAULT '0.00',
  CONSTRAINT FK_comprobante_detalle_comprobante FOREIGN KEY (Comprobante_id) REFERENCES comprobante (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_comprobante_detalle_producto FOREIGN KEY (Producto_id) REFERENCES producto (id) ON DELETE CASCADE ON UPDATE CASCADE
);

