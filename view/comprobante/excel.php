<li class="active">Comprobante #<?php echo str_pad($comprobante->id, 5, '0', STR_PAD_LEFT); ?></li>

<table>
    <tr>
        <td><b>Cliente</b></td>
        <td><b>Cedula</b></td>
        <td><b>Direccion</b></td>
    </tr>
    <tr>
        <td><?php echo $comprobante->Cliente->nombre; ?></td>
        <td><?php echo $comprobante->Cliente->ruc; ?></td>
        <td><?php echo $comprobante->Cliente->direccion; ?></td>
    </tr>
</table>
<ul id="facturador-detalle" class="list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-xs-7">
                <b>Nombre producto</b>
            </div>
            <div class="col-xs-1 text-right">
                <b>Cantidad</b>
            </div>
            <div class="col-xs-2 text-right">
                <b>Precio Unitario</b>
            </div>
            <div class="col-xs-2 text-right">
                <b>Total</b>
            </div>
        </div>
    </li>
    <?php foreach($comprobante->Detalle as $d): ?>
    <li class="list-group-item">
        <div class="row">
            <div class="col-xs-7">
                <?php echo $d->Producto->nombre; ?>
            </div>
            <div class="col-xs-1 text-right">
                <?php echo $d->cantidad; ?>
            </div>
            <div class="col-xs-2 text-right">
                <?php echo number_format($d->preciounitario, 2); ?>
            </div>
            <div class="col-xs-2 text-right">
                <?php echo number_format($d->total, 2); ?>
            </div>
        </div>
    </li>
    <?php endforeach; ?>
    <li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                Sub Total
            </div>
            <div class="col-xs-2">
                <b><?php echo number_format($comprobante->subtotal, 2); ?></b>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                IVA (18%)
            </div>
            <div class="col-xs-2">
                <b><?php echo number_format($comprobante->igv, 2); ?></b>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                Total <b>(Bs.)</b>
            </div>
            <div class="col-xs-2">
                <b><?php echo number_format($comprobante->total, 2); ?></b>
            </div>
        </div>
    </li>
</ul>
