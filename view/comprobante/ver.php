<ol class="breadcrumb">
  <li><a href="?c=Comprobante&a=index">Inicio</a></li>
  <li class="active">Comprobante #<?php echo str_pad($comprobante->id, 5, '0', STR_PAD_LEFT); ?></li>
</ol>

<div class="row">
        <div class="col-xs-12">

            <fieldset>
                <legend>Datos de nuestro cliente</legend>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Cliente</label>
                            <input type="text" class="form-control" disabled value="<?php echo $comprobante->Cliente->nombre; ?>" />
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Cedula</label>
                            <input type="text" class="form-control" disabled value="<?php echo $comprobante->Cliente->ruc; ?>"  />                    
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Dirección</label>
                            <input type="text" class="form-control" disabled value="<?php echo $comprobante->Cliente->direccion; ?>" />                    
                        </div>
                    </div>
                </div>
            </fieldset>

            <ul id="facturador-detalle" class="list-group">
                 <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-7">
                            <b>Nombre producto</b>
                        </div>
                        <div class="col-xs-1 text-right">
                            <b>Cantidad</b>
                        </div>
                        <div class="col-xs-2 text-right">
                            <b>Precio Unitario</b>
                        </div>
                        <div class="col-xs-2 text-right">
                            <b>Total</b>
                        </div>
                    </div>
                </li>
                <?php foreach($comprobante->Detalle as $d): ?>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-7">
                            <?php echo $d->Producto->nombre; ?>
                        </div>
                        <div class="col-xs-1 text-right">
                            <?php echo $d->cantidad; ?>
                        </div>
                        <div class="col-xs-2 text-right">
                            <?php echo number_format($d->preciounitario, 2); ?>
                        </div>
                        <div class="col-xs-2 text-right">
                            <?php echo number_format($d->total, 2); ?>
                        </div>
                    </div>
                </li>
                <?php endforeach; ?>
                <li class="list-group-item">
                    <div class="row text-right">
                        <div class="col-xs-10 text-right">
                            Sub Total
                        </div>
                        <div class="col-xs-2">
                            <b><?php echo number_format($comprobante->subtotal, 2); ?></b>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row text-right">
                        <div class="col-xs-10 text-right">
                            IVA (18%)
                        </div>
                        <div class="col-xs-2">
                            <b><?php echo number_format($comprobante->igv, 2); ?></b>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row text-right">
                        <div class="col-xs-10 text-right">
                            Total <b>(Bs.)</b>
                        </div>
                        <div class="col-xs-2">
                            <b><?php echo number_format($comprobante->total, 2); ?></b>
                        </div>
                    </div>
                </li>
            </ul>

        </div>
</div>

<a class="btn  btn-default btn-lg" href="?c=comprobante&a=eliminar&id=<?php echo $comprobante->id; ?>" onclick="return confirm('¿Está seguro de eliminar este comprobante?');"><i class="glyphicon glyphicon-remove"></i> Eliminar comprobante</a>
<a class="btn btn-default btn-lg" href="?c=comprobante&a=Imprimir&id=<?php echo $comprobante->id; ?>&formato=excel" onclick="return confirm('¿Está seguro de imprimir este comprobante?');"><i class="glyphicon glyphicon-print"></i> Imprimir Excel</a>
<a class="btn btn-default btn-lg" href="?c=comprobante&a=Imprimir&id=<?php echo $comprobante->id; ?>&formato=pdf" onclick="return confirm('¿Está seguro de imprimir este comprobante?');"><i class="glyphicon glyphicon-print"></i> Imprimir Pdf</a>