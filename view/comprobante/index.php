<?php 
require_once 'model/comprobante.model.php'; 
$comprobante = new ComprobanteModel();
?>
<h2 class="page-header">
    Comprobantes
    <a class="btn btn-default pull-right btn-lg" href="?c=Comprobante&a=crud">Nuevo comprobante</a>
</h2>

<style type="text/css">
  	.hoverTable{
		width:100%; 
		border-collapse:collapse; 
	}
    .hoverTable tr:hover {
          background-color: #71a0c9;
    }
</style>

 <table class="table hoverTable">
                    <thead>
                        <tr>
                            <th style="text-align:left;">Cedula</th>
                            <th style="text-align:left;">Cliente</th>
                            <th style="text-align:left;">IVA</th>
                            <th style="text-align:left;">SubTotal</th>
                            <th style="text-align:left;">Total</th>
                            <th style="text-align:left;">Ver</th>
                        </tr>
                    </thead>

                    <?php foreach($comprobante->Listar() as $b): ?>
                       	<tr>
                            <td><?php echo $b['Cedula']; ?></td>
                            <td><?php echo $b['Cliente']; ?></td>
                            <td><?php echo $b['IGV']; ?></td>
                            <td><?php echo $b['SubTotal']; ?></td>
                            <td><?php echo $b['Total']; ?></td> 
                            <td>
                               <a href="?c=comprobante&a=ver&id=<?php echo $b['Id']; ?>"><button class="btn btn-default" type="button">
                             <i class="glyphicon glyphicon-file"></i>
                        </button></a>
                            </td>
                        </tr>
                    
                    <?php endforeach; ?>
</table> 
