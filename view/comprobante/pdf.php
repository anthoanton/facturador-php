<?php 
require_once 'lib/dompdf/lib/html5lib/Parser.php';
require_once 'lib/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'lib/dompdf/lib/php-svg-lib/src/autoload.php';
require_once 'lib/dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();

$html = '<li class="active">Comprobante #'.str_pad($comprobante->id, 5, '0', STR_PAD_LEFT).'</li>
<table>
    <tr>
        <td><b>Cliente</b></td>
        <td><b>Cedula</b></td>
        <td><b>Direccion</b></td>
    </tr>
    <tr>
        <td>'.$comprobante->Cliente->nombre.'</td>
        <td>'.$comprobante->Cliente->ruc.'</td>
        <td>'.$comprobante->Cliente->direccion.'</td>
    </tr>
</table>';
$html2 = '
<table>
    <tr>
        <td><b>Producto</b></td>
        <td><b>Cantidad</b></td>
        <td><b>Precio</b></td>
        <td><b>total</b></td>
    </tr>
</table>';
foreach($comprobante->Detalle as $d):
$html3 .='
<table>
    <tr>
        <td>'.$d->Producto->nombre.'</td>
        <td>'.$d->cantidad.'</td>
        <td>'.number_format($d->preciounitario, 2).'</td>
        <td>'.number_format($d->total, 2).'</td>
    </tr>
</table>';
endforeach;
$html4 ='<ul><li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                Sub Total
            </div>
            <div class="col-xs-2">'.number_format($comprobante->subtotal, 2).'</b>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                IVA (18%)
            </div>
            <div class="col-xs-2">
                <b>'.number_format($comprobante->igv, 2).'</b>
            </div>
        </div>
    </li>
    <li class="list-group-item">
        <div class="row text-right">
            <div class="col-xs-10 text-right">
                Total <b>(Bs.)</b>
            </div>
            <div class="col-xs-2">
                <b>'.number_format($comprobante->total, 2).'</b>
            </div>
        </div>
    </li>
</ul>';


// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();

$dompdf->loadHtml($html.$html2.$html3.$html4);
// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
?>
